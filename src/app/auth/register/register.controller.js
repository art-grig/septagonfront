'use strict';

angular.module('frontend').controller('RegisterController', function ($scope, auth, $log) {
  var vm = this;

  vm.confirmEmail = function () {
    if (!auth.isAuthenticated()) {
      auth.initSession().then(function(res) {
        $log.log(res);
        confirmEmailAuth();
      });
    }
    else {
      confirmEmailAuth();
    }
  }

  vm.submit = function () {
    if (!auth.isAuthenticated()) {
      auth.initSession().then(function(res) {
        $log.log(res);
        register();
      });
    }
    else {
      register();
    }
  };

  function confirmEmailAuth() {
    auth.confirmEmail(vm.guid)
      .then(function(res) {
        $log.log(res);
        alert('Email успешно подтвержден');
      })
      .catch(function(err) {
        $log.log(err);
        alert('Ошибка при подтверждении Email');
      })
  }

  function register() {
    if (vm.password === vm.repeatedPassword) {
        auth.register({
          login: vm.login,
          domain: vm.domain,
          password: vm.password,
          email: vm.email,
          surname: vm.surname,
          name: vm.name,
          patronymic: vm.patronymic || '',
          location: vm.location || '',
          time_zone: vm.time_zone || '',
          phone: vm.phone || ''
        })
          .then(function (res) {
            $log.log(res);
            alert('Регистрация прошла успешно');
          })
          .catch(function (err) {
            $log.log(err);
            alert('Ошибка при регистрации');
          });
    } else {
      alert('Ошибка при регистрации, введенные пароли не совпадают');
    }
  }
});
