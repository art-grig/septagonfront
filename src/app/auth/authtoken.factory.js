'use strict';

angular.module('frontend').factory('authToken', function($window, $log) {
  var storage = $window.localStorage;
  var cachedToken;
  return {
    setToken: function(token) {
      cachedToken = token;
      storage.setItem('userToken', token);
      $log.log(token);
      $log.log(storage.getItem('userToken'));
    },
    getToken: function() {
      if (!cachedToken) {
        cachedToken = storage.getItem('userToken');
      }
      return cachedToken;
    },
    isAuthenticated: function() {
      return !!this.getToken();
    },
    removeToken: function() {
      storage.removeItem('userToken');
    }
  }
});
