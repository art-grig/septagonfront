angular.module('frontend').factory('authInterceptor', function($q, authToken, $injector, API_URL) {
  return {
    request: function(config) {
      var token = authToken.getToken();
      if (token && config.url.indexOf(API_URL) !== -1) {
        config.headers['X-Session-Id'] = token;
      }
      return config;
    },
    responseError: function(rejection) {
      var defer = $q.defer();

      if (rejection.status == 403) {
        if (authToken.isAuthenticated()) {
          authToken.removeToken();
        }
      }

      defer.reject(rejection);

      return defer.promise;
    }
  };
});

