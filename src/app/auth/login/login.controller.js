angular.module("frontend").controller('LoginController', function ($state, auth, $log) {
/*  var isAuthenticated = auth.isAuthenticated();
  if (isAuthenticated) {
    $state.go('home');
  }*/

  var vm = this;

  vm.submit = function () {
    if (!auth.isAuthenticated()) {
      auth.initSession().then(function(res) {
        $log.log(res);
        login();
      });
    }
    else {
      login();
    }
  };

  function login() {
    auth.login({
      login: vm.login,
      domain: vm.domain,
      password: vm.password
    })
      .then(
        function (res) {
          $log.log(res);
          alert("Вы успешно авторизированы");
        }
      )
      .catch(
        function (err) {
          $log.log(err);
          alert("Неверные данные входа");
        }
      );
  }

});
