'use strict';

angular.module('frontend').service('auth', function auth($http, API_URL, authToken, $log) {
  function authResponseValidSession(res) {
    $log.log(res);
    if (res.id) {
      authToken.setToken(res.id);
    }
  }
  function authResponseValidUser() {
  }

  function logoutResponseCheck(res) {
    if (res.status == 'success') {
      authToken.removeToken();
    }
  }

  function authRegisterSuccess() {

  }

  function emailConfirmationSuccess() {

  }

  this.isAuthenticated = function() {
    return authToken.isAuthenticated();
  };

  this.initSession = function() {
    return $http.post(API_URL + '/sessions', {
    }).success(authResponseValidSession);
  }

  this.login = function(loginModel) {
    return $http.post(API_URL + '/login', loginModel).success(authResponseValidUser);
  };

  this.register = function (registerModel) {
    return $http.post(API_URL + '/registrations', registerModel).success(authRegisterSuccess);
  }

  this.confirmEmail = function(guid) {
    return $http.post(API_URL + '/registrations/' + guid + '/confirmation',{}).success(emailConfirmationSuccess);
  }

  this.logout = function () {
    return $http.get(API_URL + 'user/logout', { }).success(logoutResponseCheck);
  };
});
