/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('frontend')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('API_URL','http://172.19.10.211/recs/api')
    .constant('IMPORT_API', 'http://172.19.10.211/recs/api/import')

})();
