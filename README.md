# README #

###Environment setup
1. sudo apt-get update
2. sudo apt-get install nodejs
3. sudo apt-get install npm
4. npm install bower -g
5. npm install gulp -g

###After cloning
* In directory contaning package.json: npm install

###Gulp tasks:
* gulp or gulp build to build an optimized version of application in /dist
* gulp serve to launch a browser sync server on source files
* gulp serve:dist to launch a server on optimized application
* gulp test to launch unit tests with Karma
* gulp test:auto to launch unit tests with Karma in watch mode
* gulp protractor to launch e2e tests with Protractor
* gulp protractor:dist to launch e2e tests with Protractor on the dist files
